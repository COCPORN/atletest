﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtleTest
{

    class Oppgave3
    {
        double Calculate(string s)
        {
            var compound = s.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            Stack<int> stack = new Stack<int>();

            foreach (var entry in compound)
            {
                switch (entry)
                {
                    case "+":
                        stack.Push(stack.Pop() + stack.Pop());
                        break;
                    case "-":
                        var previous = stack.Pop();
                        stack.Push(stack.Pop() - previous);
                        break;
                    case "*":
                        stack.Push(stack.Pop() * stack.Pop());
                        break;
                    case "/":
                        var prev = stack.Pop();
                        stack.Push(prev / stack.Pop());
                        break;
                    default:
                        stack.Push(int.Parse(entry));
                        break;
                }
            }

            return stack.Count == 0 ? 0 : stack.Peek();
        }

        public void Run()
        {
            Console.WriteLine(Calculate("5 1 2 + 4 * + 3 -"));
            Console.ReadLine();
        }
    }
}
