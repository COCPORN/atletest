﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtleTest
{
    class Oppgave1
    {
        string Iterative(string s, int max)
        {
            int counter = 0;
            char? previous = null;
            string result = String.Empty;
            for (int i = 0; i < s.Length; i++)
            {
                var c = s[i];
                if (previous.HasValue == false)
                {
                    result += c;
                    previous = c;
                }
                else
                {
                    if (c == previous.Value)
                    {
                        counter++;
                        if (counter < max)
                        {
                            result += c;
                        }
                    }
                    else
                    {
                        counter = 0;
                        result += c;
                        previous = c;
                    }
                }
            }
            return result;
        }

        public void Run()
        {
            Console.WriteLine(Iterative("abbb", 2));
            Console.WriteLine(Iterative("aabb", 1));
            Console.WriteLine(Iterative("cccaabbb", 1));
            Console.WriteLine(Iterative("aabbcc", 3));
            Console.ReadLine();

        }
    }
}
